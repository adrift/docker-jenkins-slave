FROM jenkins/jnlp-slave

ARG DC_VERSION=1.22.0

USER root
RUN apt-get update -y && apt-get upgrade -y            && \
    \
    apt-get install -qq -y --no-install-recommends \
      sudo \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg2 \
      software-properties-common                       && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
    apt-key fingerprint 0EBFCD88                       && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian stretch stable" && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian stretch stable" && \
    apt-get update                                     && \
    apt-get install -qq -y --no-install-recommends docker-ce && \
    curl -L https://github.com/docker/compose/releases/download/${DC_VERSION}/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose && \
    chmod +x /usr/local/bin/docker-compose             && \
    \
    adduser jenkins sudo                               && \
    adduser jenkins docker                             && \
    \
    curl https://bootstrap.pypa.io/get-pip.py | python && \
    pip install ansible requests-credssp               && \
    pip install pywinrm[credssp]                       && \
    \
    rm -rf /var/lib/apt/lists/*

USER jenkins

# default command: display Ansible version
#CMD [ "ansible-playbook", "--version" ]
CMD [ "/bin/bash" ]